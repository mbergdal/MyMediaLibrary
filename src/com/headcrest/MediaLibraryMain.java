package com.headcrest;

import java.util.ArrayList;

public class MediaLibraryMain {

    public static void main(String[] args) {
        ArrayList<MediaItem> allItems = new ArrayList<>();

        MediaItem theMatrix = new Movie("The Matrix");
        CompactDisk theWall = new CompactDisk("The Wall");
        Vinyl abbeyRoad = new Vinyl("Abbey Road");

        allItems.add(theMatrix);
        allItems.add(theWall);
        allItems.add(abbeyRoad);

        for (MediaItem item : allItems){
            System.out.println(item.toString());
        }
    }
}
